<?php

return [
    'emailSubject' => '¡Ya estás participando por un pack de vinos de Concha y Toro!',
    'digitalMedia' => 'Medios Digitales',
    'mailing' => 'Correo electrónico',
    'pointSales' => 'Punto de venta',
    'externalAdvertising' => 'Publicidad exterior',
    'invalidAge' => 'Para concursar debes ser mayor de 18 años',
    'invalidDate' => 'La fecha ingresada no es valida',
    'invalidAllFields' => 'Debes completar todos los campos',
];
