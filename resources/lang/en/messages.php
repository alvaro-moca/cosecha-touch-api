<?php

return [
    'emailSubject' => 'You’re competing for a pack of Concha y Toro wines!',
    'digitalMedia' => 'Social Media',
    'mailing' => 'Newsletter',
    'pointSales' => 'Point of sales',
    'externalAdvertising' => 'Advertising',
    'invalidAge' => 'You have to be over 18 to enter in the contest',
    'invalidDate' => 'The birth date is not valid',
    'invalidAllFields' => 'You must complete all the fields',
];
