<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title></title>
    <style type="text/css">body,td,th {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 12px;
            color: #000;
        }
        body {
            background-color: #FFF;
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
    </style>
</head>

<body>
<table width="800" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto; background-color: #121212;">
    <tr>
        <td><img src="{{$assetsUrl}}/img/cyt_01-eng.jpg" alt="An Extraordinary 2018 Vintage" width="800" height="329" border="0" style="display:block" /></td>
    </tr>
    <tr>
        <td style="height: 35px;">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:100px;">&nbsp;</td>
                    <td style="width:400px; text-align: center;">
                        <img src="{{$assetsUrl}}/img/cyt_02-eng.jpg" alt="An Extraordinary 2018 Vintage" width="800" height="269" border="0" style="display:block" />
                    </td>
                    <td style="width:100px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:100px;">&nbsp;</td>
                    <td style="width:400px; text-align: center;">
                        <span style="color:#A89E83; display: block; text-transform:uppercase; font-size: 16px; font-style: italic;">Follow Us At</span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="height: 10px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width:140px;">&nbsp;</td>
                                <td style="width:40px;"><a style="display:block; margin: 0 auto; width:22px;" href="https://www.facebook.com/conchaytoro/" target="_blank" title="Facebook"><img src="{{$assetsUrl}}/img/cyt_06.jpg" alt="Facebook" width="22" height="21" border="0" style="display:block" /></a></td>
                                <td style="width:40px; text-align: center;"><a style="display:block; margin: 0 auto; width:22px;" href="https://instagram.com/conchaytoro" target="_blank" title="Instagram"><img src="{{$assetsUrl}}/img/cyt_08.jpg" alt="Instagram" width="22" height="21" border="0" style="display:block" /></a></td>
                                <td style="width:40px;"><a style="display:block; margin: 0 auto; width:22px;" href="https://www.youtube.com/user/conchaytorovina" target="_blank" title="Youtube"><img src="{{$assetsUrl}}/img/cyt_10.jpg" alt="Youtube" width="22" height="21" border="0" style="display:block" /></a></td>
                                <td style="width:140px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width:100px;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 35px;">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto; background-color: #ffffff;">
                <tr>
                    <td style="height: 35px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:100px;">&nbsp;</td>
                    <td style="width:400px; text-align: center;">
                        <span style="color:#1F1F1F; margin-bottom: 10px; display: block; text-transform:uppercase; font-size: 22px;">How did you find out about the campaign?</span>
                    </td>
                    <td style="width:100px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:200px;">&nbsp;</td>
                    <td style="width:400px; text-align: center;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="height: 10px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><a style="display:block; text-align:center; background-color:#F2F2F2; padding:10px; color:#1F1F1F; font-size: 16px; text-decoration: none; margin-bottom: 15px;" href="{{$appUrl}}/success/?key={{$id}}&ref={{$digitalMedia}}" target="_blank" title="Social Media (Facebook, Instagram, Youtube)">Social Media (Facebook, Instagram, Youtube)</a></td>
                            </tr>
                            <tr>
                                <td><a style="display:block; text-align:center; background-color:#F2F2F2; padding:10px; color:#1F1F1F; font-size: 16px; text-decoration: none; margin-bottom: 15px;" href="{{$appUrl}}/success/?key={{$id}}&ref={{$mailing}}" target="_blank" title="Newsletters">Newsletters</a></td>
                            </tr>
                            <tr>
                                <td><a style="display:block; text-align:center; background-color:#F2F2F2; padding:10px; color:#1F1F1F; font-size: 16px; text-decoration: none; margin-bottom: 15px;" href="{{$appUrl}}/success/?key={{$id}}&ref={{$pointSales}}" target="_blank" title="Point of Sales (Supermarkets, Wine Stores)">Point of Sales (Supermarkets, Wine Stores)</a></td>
                            </tr>
                            <tr>
                                <td><a style="display:block; text-align:center; background-color:#F2F2F2; padding:10px; color:#1F1F1F; font-size: 16px; text-decoration: none;" href="{{$appUrl}}/success/?key={{$id}}&ref={{$externalAdvertising}}" target="_blank" title="Advertising">Advertising</a></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width:200px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 35px;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><a target="_blank" href="https://www.youtube.com/watch?v=oPh1MJJ5m6E"><img src="{{$assetsUrl}}/img/cyt_14.jpg" alt="Concha y Toro" width="800" height="452" border="0" style="display:block" /></a></td>
    </tr>
    <tr>
        <td><img src="{{$assetsUrl}}/img/cyt_15.jpg" alt="Concha y Toro" width="800" height="66" border="0" style="display:block" /></td>
    </tr>
    <tr>
        <td style="height: 40px; background-color: #121212;">&nbsp;</td>
    </tr>
</table>
</body>
</html>
