<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use Illuminate\Support\Facades\DB;

class FormColumnFilter extends Filter
{
    public $component = 'select-filter';

    /**
     * The filter's component.
     *
     * @var string
     */
    //public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('country', $value);


        // return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function options(Request $request)
    {
        $values = [];
        $forms = DB::table('forms')
            ->groupBy('country')
            ->get();

        foreach ($forms as $form) {
            $values[$form->country] = $form->country;
        }

        return $values;
    }
}
