<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Victorybiz\GeoIPLocation\GeoIPLocation;

class LocationController extends Controller
{
    public function getLocation()
    {
        $geoip = new GeoIPLocation();
        return [
          'name' => $geoip->getCountry(),
          'code' => $geoip->getCountryCode(),
        ];
    }
}
