<?php
namespace App\Http\Controllers;
ini_set('display_errors',1);

use Illuminate\Support\Facades\Crypt;
use App\Form;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'email|required',
                'country' => 'required',
                'language' => 'required',
                'score' => 'required',
                'tos' => 'required',
                'birthdate_day' => 'required',
                'birthdate_month' => 'required',
                'birthdate_year' => 'required',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'showUser' => false,
                'msg' => 'Debes completar todos los campos',
                'code' => 'FRM-STORE-01'
            ], 400);
        }

        $locale = preg_replace("/[^a-zA-Z0-9]+/", "", substr(strtolower($request->get('language')), 0, 2));
        $birth_date = $request->get('birthdate_year') . '-' . $request->get('birthdate_month') . '-' . $request->get('birthdate_day');
        try {
            $age = Carbon::parse($birth_date)->age;

            if ($age < 18) {
                return response()->json([
                    'showUser' => true,
                    'msg' => __('messages.invalidAge', [], $locale),
                    'code' => 'FRM-STORE-04'
                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                'showUser' => true,
                'msg' => __('messages.invalidDate', [], $locale),
                'code' => 'FRM-STORE-01'
            ], 400);
        }

        $form = new Form();
        $form->first_name = $request->get('first_name');
        $form->last_name = $request->get('last_name');
        $form->email = $request->get('email');
        $form->birthdate = $birth_date;
        $form->accept_tos = $request->get('tos');
        $form->country = $request->get('country');
        $form->language = $request->get('language');
        $form->score = $request->get('score');
        $form->answers = $request->get('answers');
        $form->notes = $request->get('notes');
        $form->phone = $request->get('phone');
        $form->ref = "";

        try {
            $form->save();
        } catch (\Exception $e) {
            return response()->json([
                'showUser' => false,
                'msg' => "Error guardando en la base de datos " . (env('APP_DEBUG') ? $e->getMessage() : ''),
                'code' => 'FRM-STORE-02'
            ], 500);
        }

        if (!$form->id) {
            return response()->json([
                'showUser' => false,
                'msg' => "Error guardando en la base de datos",
                'code' => 'FRM-STORE-05'
            ], 500);
        }

        /*try {
            Mail::send("emails.success-" . $locale, [
                'assetsUrl' => env('ASSETS_URL'),
                'appUrl' => env('APP_URL'),
                'id' => urlencode(Crypt::encryptString($form->id)),
                'digitalMedia' => urlencode(Crypt::encryptString(__('messages.digitalMedia', [], $locale))),
                'mailing' => urlencode(Crypt::encryptString(__('messages.mailing', [], $locale))),
                'pointSales' => urlencode(Crypt::encryptString(__('messages.pointSales', [], $locale))),
                'externalAdvertising' => urlencode(Crypt::encryptString(__('messages.externalAdvertising', [], $locale))),
            ], function ($message) use ($locale, $form) {
                /**
                 * @var \Illuminate\Mail\Message $message
                 
                $subject = __('messages.emailSubject', [], $locale);
                $message->to($form->email);
                $message->subject($subject);
            });
        } catch (\Exception $e) {
            return response()->json([
                'showUser' => false,
                'msg' => 'Error enviando email',
                'code' => 'FRM-STORE-03'
            ], 500);
        }*/

        return $form;
    }
}
