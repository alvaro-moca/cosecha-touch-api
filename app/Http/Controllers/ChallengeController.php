<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function success(Request $request)
    {
        $view = 'success-challenge-en';
        try {
            $id = Crypt::decryptString($request->input('key'));
            /** @var Form $form */
            $form = Form::where('id', '=', $id)->firstOrFail();
            $viewExists = view()->exists('success-challenge-' . $form->language);

            if ($viewExists) {
                $view = 'success-challenge-' . $form->language;
            }

            if (trim($form->ref)) return view($view);

            $form->ref = Crypt::decryptString(urldecode($request->input('ref')));
            $form->save();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return view($view);
    }
}
